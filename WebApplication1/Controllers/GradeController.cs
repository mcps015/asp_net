﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class GradeController : Controller
    {
        // GET: Grade
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(int number)
        {
            string level = "";
            if (number <=100 && number >=80)
            {
                level = "A";
            }
            else if (number < 80 && number >= 60)
            {
                level = "B";
            }
            else if (number < 60 && number >= 40)
            {
                level = "C";
            }
            else if (number < 40 && number >= 20)
            {
                level = "D";
            }
            else if (number < 20 && number >= 0)
            {
                level = "E";
            }
            else
            {
                level = "Please enter the number between 0-100";
            }
            ViewBag.number = number;
            ViewBag.level = level;

            return View();
        }
    }
}