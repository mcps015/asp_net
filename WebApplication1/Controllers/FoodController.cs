﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class FoodController : Controller
    {
        // GET: Food
        public ActionResult Index()
        {
            string[] fruits = { "蘋果", "香蕉", "荔枝", "鳳梨", "西瓜" };

            ViewBag.Data = fruits;
            ViewData["Data"] = fruits;

            return View();
        }
    }
}